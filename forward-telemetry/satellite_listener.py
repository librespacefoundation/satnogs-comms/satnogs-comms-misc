#!/usr/bin/env python3
""" SatNOGS COMMS frame forwarder from flowgraphs to satnogs db """

import argparse
import requests
import zmq
import pmt


# Need to suggest satellite in db-dev: https://db-dev.satnogs.org/satellites/
SCID_to_NORAD_ID = {
  1: 99996
}

def sids_submit_frame(args, norad_id, timestamp, data):
    params = {
        'noradID': norad_id,
        'source': args.callsign,
        'timestamp': timestamp,
        'locator': 'longLat',
        'longitude': args.lon,
        'latitude': args.lat,
        'frame': data,
    }

    try:
        response = requests.post(args.telemetry_url, data=params, timeout=120)
        response.raise_for_status()
    except requests.HTTPError as e:
        print("ERROR: Frame forwarding failed, status code: {}".format(response.status_code))


def packet_listener_and_forwarder(args):
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect(args.zmq_pub_uri)
    socket.setsockopt_string(zmq.SUBSCRIBE, '')

    print ("Listening on GRC ZMQ {}".format(args.zmq_pub_uri))

    while True:
        try:
            # Poll for incoming messages
            socket.poll(timeout=50000)  # Wait for up to 50 second, consider that 30sec is the periodic telemetry
            # Receive message
            raw_data = socket.recv()
            # Deserialize the message using PMT functions
            frame = pmt.to_python(pmt.deserialize_str(raw_data))
            metadata = frame[0]
            pdu = ''.join([hex(i)[2:] for i in frame[1]])
            timestamp = metadata['time']
            print(timestamp)
            print(pdu)
            # TODO: When the decoder is available need to check the decoded data
        except Exception as e:
            print(f"Error: {e}")

        if args.telemetry_url == "":
            # Skip telememtry forwarding
            continue

        # Send frame to satnogs-db
        sids_submit_frame(args, SCID_to_NORAD_ID[1], timestamp, pdu)

# If you have katai struct on satnogs-decoder you must connect in the DB the decoder
# If you do not have katai struct please open an issue on satnogs-decoder, then the above process
# it will run form satnogs community
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Listen for frames via UDP and forward them to satnogs-db.')
    parser.add_argument('--callsign', type=str,
                        default="ZZ0ZZZ",
                        help='Callsign, e.g. ZZ0ZZZ')
    parser.add_argument('--lat', type=str,
                        default="23.50000E",
                        help='Latitude, e.g. 23.50000E')
    parser.add_argument('--lon', type=str,
                        default="53.25000N",
                        help='Longitude, e.g. 53.25000N')
    parser.add_argument('--zmq_pub_uri', type=str,
                        default="tcp://127.0.0.1:55000",
                        help='GRC ZMQ, e.g. tcp://127.0.0.1:55000')
    parser.add_argument('--telemetry_url', type=str,
                        default="",
                        help='SatNOGS DB API Telementry endpoint, e.g. https://db-dev.satnogs.org/api/telemetry/ ; default: \"\"')

    args = parser.parse_args()

    try:
        # Check here:
        # https://db-dev.satnogs.org/api/telemetry/?satellite=99996&app_source=&observer=&transmitter=&is_decoded=&sat_id=&start=&end=
        # for the packets, need to have log in to https://db-dev.satnogs.org/
        # Also change in URL the NORAD ID
        packet_listener_and_forwarder(args)
    except KeyboardInterrupt:
        print("Aborted.")
