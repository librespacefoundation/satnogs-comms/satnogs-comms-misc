# Satellite Listener

Listen for frames from the [flowgraph](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-software-mcu/-/tree/master/contrib/flowgraphs?ref_type=heads) via ZMQ and forward them to satnogs-db. The script is based on [QUBIK Listener](https://gitlab.com/librespacefoundation/qubik/qubik-listener).

## Installation

In order to run this script you need to have GNU Radio Companion 3.10,
because it uses https://wiki.gnuradio.org/index.php/Polymorphic_Types_(PMTs)
Also it needs to install "python3-zmq" and "python3-requests" in to your system.
Otherwise, you can create a python virtual environment and install the dependencies
inside, but you need to enable "--system-site-packages" in order to have "gnuradio-pmt"

## Usage

```
usage: satellite_listener.py [-h] [--callsign CALLSIGN] [--lat LAT] [--lon LON] [--zmq_pub_uri] [--telemetry_url TELEMETRY_URL]

Listen for frames via ZMQ and forward them to satnogs-db.

optional arguments:
  -h, --help            show this help message and exit
  --callsign CALLSIGN   Callsign, e.g. ZZ0ZZZ
  --lat LAT             Latitude, e.g. 23.50000E
  --lon LON             Longitude, e.g. 53.25000N
  --zmq_pub_uri         GRC ZMQ, e.g. tcp://127.0.0.1:55000
  --telemetry_url TELEMETRY_URL
                        SatNOGS DB API Telementry endpoint, e.g. https://db-dev.satnogs.org/api/telemetry/ ; default: ""
```

### SatNOGS

The 1st step is to suggest your satellite in to [SatNOGS DB-dev](https://db-dev.satnogs.org/)
Then you must update this part of source code:
```
# Need to suggest satellite in db-dev: https://db-dev.satnogs.org/satellites/
SCID_to_NORAD_ID = {
  1: 99996
}
```
To include your satellite NORAD ID

You can check your bitstream you must log in SatNOGS DB-dev and then you can check the [SatNOGS DB API](https://db-dev.satnogs.org/api/telemetry/?satellite=99996&app_source=&observer=&transmitter=&is_decoded=&sat_id=&start=&end=)
in the URL change the NORAD ID.

Now you are ready to create an issue at [satnogs-decoders repository](https://gitlab.com/librespacefoundation/satnogs/satnogs-decoders) to describe the structure of the frame and develop the kaitai structure.

### GNU Radio Companion

## License

[![license](https://img.shields.io/badge/license-AGPL%203.0-6672D8.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202014--2024-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)
