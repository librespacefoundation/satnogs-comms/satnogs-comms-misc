# Misc

## A100-AT86-LVDS-breakout

Breakout for connecting Arty-7 FPGA devboard with ATREB215-xpro Extension Board

## at86-LVDS-test

Artix-7 FPGA project in vivado for testing AT86RF215 I/Q

## AGC Development Board

AGC Development Board is based on VGA ADL5330 and detector AD8318.
Requires [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib) to be installed.

## RF Mixer with integrated LO

RF mixer development board with integrated LO. The board is based on RFFC2071.
Requires [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib) to be installed.

## LNA for UHF
LNA for UHF, based on TQP3M9036 and (optionally) on BPF Taoglas DBP.433.T.A.30.
Also the board contains ESD protection (optionally), Littelfuse PGB1010603MR.
Requires [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib) to be installed.

## RF Probe
RF Probe, it is used to calibrate and debug RFFE of SatNOGS COMMS Board.
Instead to get input/output in AT86RF215 it gets input/output in SA/SG.
Requires [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib) to be installed.

## Clock Tree
Network synchronizer clocks supporting 1 PPS input and output, based on Si5384.

## Ethernet Adapter
Ethernet adapter for FFC to [development board of TI](https://www.ti.com/lit/ug/snlu190/snlu190.pdf?HQS=dis-mous-null-mousermode-dsf-pf-null-wwe&ts=1700574406180&ref_url=https%253A%252F%252Fgr.mouser.com%252F) based on DP83867ERGZ. The used PCB stackup is [JLC04161H-7628](https://jlcpcb.com/impedance).

## MCU Programming Breakout Board
Breakout board used to help program the FPGA. It breaks-out the [JLink-EDU](https://www.segger.com/products/debug-probes/j-link/technology/interface-description/) to a [10-pin picoblade connector](https://www.molex.com/en-us/products/part-detail/532611071?display=pdf) that mates with the JTAG MCU connector on SatNOGS COMMS. It is based on TC2050-ARM2010 ARM 20-pin to TC2050 adapter. The used PCB stackup is [JLC04161H-7628](https://jlcpcb.com/impedance).

## FPGA Programming Breakout Board
Breakout board used to help program the FPGA. It breaks-out the [JTAG-HS3](https://digilent.com/reference/programmers/jtag-hs3/reference-manual) to a [10-pin picoblade connector](https://www.molex.com/en-us/products/part-detail/532611071?display=pdf) that mates with the JTAG FPGA connector on SatNOGS COMMS. It is based on TC2050-XILINX-M. The used PCB stackup is [JLC04161H-7628](https://jlcpcb.com/impedance).

## Forward Telemetry
SatNOGS COMMS frame forwarder from flowgraphs to SatNOGS DB. For details: [forward-telemetry](forward-telemetry/README.md).

## License
Licensed under the [CERN OHLv1.2](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202014--2020-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)
